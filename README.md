# serverless-handson

Various support artifacts for the Microservices and Serverless course.

**Make sure to visit the _Snippets_ area in this repository project within Gitlab
for useful links and files needed for the course.**

https://gitlab.com/coveros/microservice_serverless/serverless-handson/-/snippets

This training session makes heavy use of the SecureCI demo instance and the CodeVeros demo 
application that are maintained by Coveros.

You can download the repository for SecureCI here:

https://gitlab.com/coveros/secureci/securecidemo-jenkins-k8s

CodeVeros is available from the class-based Gitlab fork as well as it's primary home in
Github.

https://gitlab.com/coveros/microservice_serverless/codeveros - class-based home

https://github.com/Coveros/codeveros - Github development home

